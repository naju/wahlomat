import Vuex from 'vuex'
import Vue from 'vue'

let data = {}
if (process.env.NODE_ENV === 'production')
  data = document.wahlomatData
else
  data = require('../data.json')

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    data: { ...data },
    page: 'hello',
    question: 0,
    finished: false,
    answers: data.questions.map(() => null)
  },
  mutations: {
    goToPage (state, nextPage) {
      state.page = nextPage
    },
    goToQuestion (state, question) {
      state.question = question
    },
    changeAnswer (state, { question, answer }) {
      let newAnswers = [...state.answers]
      newAnswers[question] = answer
      state.answers = newAnswers
    },
    finish (state) {
      state.finished = true
    }
  },
  actions: {
    goToPage ({ commit }, page) {
      commit('goToQuestion', 0)
      commit('goToPage', page)
    },
    goToQuestion ({ commit }, question) {
      commit('goToPage', 'questions')
      commit('goToQuestion', question)
    },
    answerAndNext ({ state, commit }, { question, answer }) {
      commit('changeAnswer', { question, answer })
      let next = question + 1
      if (next < state.data.questions.length)
        commit('goToQuestion', next)
      else {
        commit('finish')
        commit('goToPage', 'result')
      }
    }
  }
})

