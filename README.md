# NAJU Wahl-O-Mat

Die NAJU hat jetzt einen Wahl-O-Mat. Er ist CI-konform und lässt sich nach dem Build direkt in eine Website kopieren.

## Build Setup

``` bash
# install dependencies
yarn

# serve with hot reload at localhost:8080
yarn dev

# build for production with minification
yarn build

# build for production and view the bundle analyzer report
yarn build --report
```

For detailed explanation on how things work, checkout the [guide](http://vuejs-templates.github.io/webpack/) and [docs for vue-loader](http://vuejs.github.io/vue-loader).

## Injection
Nach dem Build:

1. Den Code im `<body>` der `./dist/index.html` kopieren und auf die vorbereitete Seite einfügen.
2. Den Code aus der JavaScript-Datei im Ordner `./dist/static/js/` kopieren und unter den Code von eben einfügen.
3. Speichern.
4. Freuen.

## Konfiguration
Die Daten befinden sich in der Datei `.src/data.json`.
Diese definiert scih wie folgt:

```javascript
type data {
  questions: Array<question>,
  parties: Array<party>,
  statements_url?: String<URL>
}

type question {
  question: String,
  link?: String,
  short: String,
  type: enum<String>("binary", "range"),
  category: String
}

type party {
  name: String,
  color: String<CssColor>,
  answers: Array<Number>
}
```

Bei Fragen des Typs `range` werden die Zahlen `{0, 1, 2}` akzeptiert.
Bei Fragen des Typs `binary` werden die Zahlen `{0, 1}` akzeptiert.
Falls eine Antwort neutral ausfällt oder die Frage "nicht Bestandteil der Überlegungen war, muss der Wert `-1` eingetragen werden.
Die genaue Zuordnung der Werte erschließen sich aus dem Template in `Questions.vue`

In der `statements_url` kann eine URL angegeben werden, unter der sich die Stellungnahmen befinden.
